
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

public class Azure {

	public static void main(String[] args) throws Exception {
       

        String pipelineRunning = (String) "cmd /C powershell Invoke-AzDataFactoryV2Pipeline -ResourceGroupName DevSandBox -DataFactoryName CustomerADFDataopsDemo -PipelineName customerpipeline";

        String uploadData = (String) "cmd /C az storage blob upload -f \"C:\\Users\\235399\\OneDrive - Cognizant\\DigitalEngg\\AzurePipelineAcademy\\Customer.txt\" -c devdataopscustomer -n Customer.txt --account-name devdatasourcedataops";
        
        new Azure().executeCommand(uploadData);
        new Azure().executeCommand(pipelineRunning);

	}
	
	public int returnData(int data)
	{
		return data;
	}
	
	public void executeCommand(String command) throws Exception
	{
		Runtime runtime = Runtime.getRuntime();
		Process proc = runtime.exec(command);

		proc.getOutputStream().close();

		InputStream inputstream = proc.getInputStream();

		InputStreamReader inputstreamreader = new InputStreamReader(inputstream);

		BufferedReader bufferedreader = new BufferedReader(inputstreamreader);

		String line;

		while ((line = bufferedreader.readLine()) != null) {

			System.out.println(line);

		}
	}
}
